package dnl.com.sns_ui.fragment;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import dnl.com.sns_ui.R;
import dnl.com.sns_ui.adapter.SearchAdapter;
import dnl.com.sns_ui.model.Header;
import dnl.com.sns_ui.model.Item;
import dnl.com.sns_ui.model.SearchListItem;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HomeFM.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomeFM#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFM extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private ListView lvSearch;
    private SearchView searchView;

    public HomeFM() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFM.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFM newInstance(String param1, String param2) {
        HomeFM fragment = new HomeFM();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_fm, container, false);
        lvSearch = (ListView) view.findViewById(R.id.lvSearch);
        searchView= (SearchView) view.findViewById(R.id.searchView);
        final List<Item> items = new ArrayList<Item>();
        items.add(new Header("Systems"));
        items.add(new SearchListItem("Engma", "http://i.imgur.com/DvpvklR.png", 0, getContext()));
        items.add(new SearchListItem("NDS.TS", "http://i.imgur.com/DvpvklR.png", 1, getContext()));
        items.add(new SearchListItem("Vina", "http://i.imgur.com/DvpvklR.png", 1, getContext()));
        items.add(new SearchListItem("VTC", "http://i.imgur.com/DvpvklR.png", 0, getContext()));
        items.add(new Header("Groups"));
        items.add(new SearchListItem("Mobile", "http://i.imgur.com/DvpvklR.png", 0, getContext()));
        items.add(new Header("Friends"));
        items.add(new SearchListItem("Nguyen Anh Tuan", "http://i.imgur.com/DvpvklR.png", 0, getContext()));
        items.add(new SearchListItem("Le Minh", "http://i.imgur.com/DvpvklR.png", 0, getContext()));
        items.add(new SearchListItem("Nguyen Anh Tuan", "http://i.imgur.com/DvpvklR.png", 0, getContext()));
        items.add(new SearchListItem("Le Minh", "http://i.imgur.com/DvpvklR.png", 0, getContext()));
        items.add(new SearchListItem("Nguyen Anh Tuan", "http://i.imgur.com/DvpvklR.png", 0, getContext()));
        items.add(new SearchListItem("Le Minh", "http://i.imgur.com/DvpvklR.png", 0, getContext()));
        items.add(new SearchListItem("Nguyen Anh Tuan", "http://i.imgur.com/DvpvklR.png", 0, getContext()));
        items.add(new SearchListItem("Le Minh", "http://i.imgur.com/DvpvklR.png", 0, getContext()));
        items.add(new SearchListItem("Nguyen Anh Tuan", "http://i.imgur.com/DvpvklR.png", 0, getContext()));
        items.add(new SearchListItem("Le Minh", "http://i.imgur.com/DvpvklR.png", 0, getContext()));
        items.add(new SearchListItem("Nguyen Anh Tuan", "http://i.imgur.com/DvpvklR.png", 0, getContext()));
        items.add(new SearchListItem("Le Minh", "http://i.imgur.com/DvpvklR.png", 0, getContext()));
        final SearchAdapter adapter = new SearchAdapter(getContext(), items);
        lvSearch.setAdapter(adapter);
        lvSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                SearchListItem searchListItem = (SearchListItem) items.get(i);
                Toast.makeText(getContext(), searchListItem.toString(), Toast.LENGTH_SHORT).show();
            }
        });

        int searchPlateId = searchView.getContext().getResources().getIdentifier("android:id/search_plate", null, null);
        View searchPlate = view.findViewById(searchPlateId);
        searchPlate.setBackgroundResource(R.drawable.bg_white_background);
        int searchSrcTextId = getResources().getIdentifier("android:id/search_src_text", null, null);
        EditText searchEditText = (EditText) searchView.findViewById(searchSrcTextId);
        searchEditText.setTextColor(Color.WHITE);
        searchEditText.setHintTextColor(Color.GRAY);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
