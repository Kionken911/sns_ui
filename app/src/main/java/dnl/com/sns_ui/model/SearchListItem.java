package dnl.com.sns_ui.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import dnl.com.sns_ui.R;
import dnl.com.sns_ui.adapter.SearchAdapter;

/**
 * Created by Dev.TuanNguyen on 7/14/2017.
 */

public class SearchListItem implements Item {
    private final String nameStr;
    private final String avatarUrl;
    private final int status;
    private Context context;

    public SearchListItem(String nameStr, String avatarUrl, int status, Context context) {
        this.nameStr = nameStr;
        this.avatarUrl = avatarUrl;
        this.status = status;
        this.context = context;
    }

    @Override
    public int getViewType() {
        return SearchAdapter.RowType.LIST_ITEM.ordinal();
    }

    @Override
    public View getView(LayoutInflater inflater, View convertView) {
        View view;
        if (convertView == null) {
            view = (View) inflater.inflate(R.layout.item_list, null);
            // Do some initialization
        } else {
            view = convertView;
        }

        TextView tvName = (TextView) view.findViewById(R.id.tvName);
        ImageView imgAvatar = (ImageView) view.findViewById(R.id.imgAvatar);
        ImageView imgStatus = (ImageView) view.findViewById(R.id.imgStatus);

        Picasso.with(context).load(avatarUrl).into(imgAvatar);
        if (status == 0) {
            Picasso.with(context).load(R.mipmap.ic_loop_black_24dp).into(imgStatus);
        } else {
            Picasso.with(context).load(R.mipmap.ic_group_black_24dp).into(imgStatus);
        }

        tvName.setText(nameStr);


        return view;
    }

    @Override
    public String toString() {
        String itemInfo = "Name: " + nameStr + '\n' + "Status: " + status;
        return itemInfo;
    }
}
