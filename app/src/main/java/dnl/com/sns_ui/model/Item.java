package dnl.com.sns_ui.model;

import android.view.LayoutInflater;
import android.view.View;

/**
 * Created by Dev.TuanNguyen on 7/14/2017.
 */

public interface Item {
    public int getViewType();
    public View getView(LayoutInflater inflater, View convertView);
}
