package dnl.com.sns_ui.model;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import dnl.com.sns_ui.R;
import dnl.com.sns_ui.adapter.SearchAdapter;

/**
 * Created by Dev.TuanNguyen on 7/14/2017.
 */

public class Header implements Item {
    private final String         name;

    public Header(String name) {
        this.name = name;
    }

    @Override
    public int getViewType() {
        return SearchAdapter.RowType.HEADER_ITEM.ordinal();
    }

    @Override
    public View getView(LayoutInflater inflater, View convertView) {
        View view;
        if (convertView == null) {
            view = (View) inflater.inflate(R.layout.header, null);
            // Do some initialization
        } else {
            view = convertView;
        }

        TextView text = (TextView) view.findViewById(R.id.separator);
        text.setText(name);

        return view;
    }

}
