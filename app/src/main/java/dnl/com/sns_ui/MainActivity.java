package dnl.com.sns_ui;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import dnl.com.sns_ui.adapter.ViewPagerAdapter;
import dnl.com.sns_ui.fragment.FriendFM;
import dnl.com.sns_ui.fragment.HomeFM;
import dnl.com.sns_ui.fragment.MessageFM;
import dnl.com.sns_ui.fragment.NotificationFM;
import dnl.com.sns_ui.fragment.SettingFM;

public class MainActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private List<Integer> iconTabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addControl();
        addEvent();

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.insertNewFragment(new HomeFM());
        adapter.insertNewFragment(new FriendFM());
        adapter.insertNewFragment(new MessageFM());
        adapter.insertNewFragment(new NotificationFM());
        adapter.insertNewFragment(new SettingFM());
        viewPager.setAdapter(adapter);
    }

    private void addEvent() {
        initIconList();
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        setTabIcon();

    }

    private void setTabIcon() {
        for (int i = 0; i < iconTabs.size(); i++) {
            tabLayout.getTabAt(i).setIcon(iconTabs.get(i));
        }
        tabLayout.getTabAt(0).select();
    }

    private void initIconList() {
        iconTabs = new ArrayList<>();
        iconTabs.add(R.mipmap.ic_home_black_24dp);
        iconTabs.add(R.mipmap.ic_group_black_24dp);
        iconTabs.add(R.mipmap.ic_message_black_24dp);
        iconTabs.add(R.mipmap.ic_notifications_black_24dp);
        iconTabs.add(R.mipmap.ic_settings_black_24dp);

    }

    private void addControl() {
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        viewPager = (ViewPager) findViewById(R.id.main_tab_content);
    }
}
