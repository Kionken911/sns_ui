package dnl.com.sns_ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;

import dnl.com.sns_ui.R;
import dnl.com.sns_ui.model.Item;

/**
 * Created by Dev.TuanNguyen on 7/14/2017.
 */

public class SearchAdapter extends ArrayAdapter<Item> {
    private LayoutInflater mInflater;
    private static final int TYPE_ITEM = 0;
    private static final int TYPE_SEPARATOR = 1;

    public enum RowType {
        LIST_ITEM, HEADER_ITEM
    }

    public SearchAdapter(Context context, List<Item> items) {
        super(context, 0, items);
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getViewTypeCount() {
        return RowType.values().length;

    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).getViewType();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        int rowType = getItemViewType(position);
        View View;
        holder = new ViewHolder();
        switch (rowType) {
            case TYPE_ITEM:
                convertView = mInflater.inflate(R.layout.item_list, null);
                holder.View = getItem(position).getView(mInflater, convertView);
                break;
            case TYPE_SEPARATOR:
                convertView = mInflater.inflate(R.layout.header, null);
                holder.View = getItem(position).getView(mInflater, convertView);
                break;
        }
        convertView.setTag(holder);
        return convertView;
    }

    public static class ViewHolder {
        public View View;
    }

}
